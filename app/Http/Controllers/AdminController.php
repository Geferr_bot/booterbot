<?php

namespace App\Http\Controllers;

use App\TechCategories;
use App\Tech;
use Illuminate\Http\Request;
use App\Work;
use App\Slide;

class AdminController extends Controller
{
    private $data = [];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->data['counts']['skills'] = Tech::count();
        $this->data['counts']['works'] = Work::count();
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [];
        $data['info'] = $this->data;

        $data['works'] = Work::limit(3)->latest()->get();
        $data['techCat'] = TechCategories::latest()->get();
        $data['slides'] = Slide::latest()->get();
        $techs = Tech::get();
        $data['techs'] = [];
        foreach ($techs as $tech){
            $data['techs'][$tech->id]['id'] = $tech->id;
            $data['techs'][$tech->id]['category_id'] = $tech->category_id;
            $data['techs'][$tech->id]['name'] = $tech->name;
            $data['techs'][$tech->id]['skill'] = $tech->skill;
            $data['techs'][$tech->id]['skill-color'] = $this->getColor($tech->skill);
        }


        return view('admin-index', $data);
    }
    private function getColor($perc){
        if($perc > 0 && $perc <= 25){
            return 'progress-danger';
            exit;
        }elseif($perc > 25 && $perc <= 50){
            return 'progress-warning';
            exit;
        }elseif($perc > 51 && $perc <= 75){
            return '';
            exit;
        }elseif($perc > 76 && $perc <= 99){
            return 'progress-success';
            exit;
        }else{
            return '';
        }
    }
    public function ListPosts()
    {
        return view('admin-posts');
    }
    public function ListWorks()
    {
        $data = [];
        $data['info'] = $this->data;
        $data['works'] = Work::latest()->get();
        return view('admin-works', $data);
    }
    public function Skills()
    {
        return view('admin-skills');
    }

}
