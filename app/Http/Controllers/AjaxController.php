<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Work;
use App\TechCategories;
use App\Tech;
use App\Slide;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;

class AjaxController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function addWork(Request $request){

        $validator = Validator::make($request->all(), [
            'title' => 'required|unique:pfworks|max:255',
            'slug' => 'required',
        ]);

        if ($validator->fails()) {
            return response($validator->errors(), 401);
        }

        $data = $request->all();
        unset($data['_token']);
        $data['thumbnail'] = '';
        $work = Work::create($data);
        //save image
        if(isset($request->thumbnail) && !empty($request->thumbnail)){
            $ext = $request->thumbnail->getClientOriginalExtension();
            if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
                try {
                    $fileName = 'wi_'.rand(100, 999999).'_'.$work->id.'.'.$ext;
                    $request->thumbnail->move(public_path('img/content/'), $fileName);
                    Work::where('id', $work->id)->update(['thumbnail' => $fileName]);
                }catch(\Exception $e){
                    return $e;
                }
            }else{
                return 'photo extension only: jpg, jpeg, png';
            }
        }
        return $work;
    }
    public function addSlide(Request $request){
        if(isset($request->slide) && !empty($request->slide)){
            $ext = $request->slide->getClientOriginalExtension();
            if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
                try {
                    $fileName = 'sl_'.rand(100, 999999).'_'.Auth::id().'.'.$ext;
                    $request->slide->move(public_path('img/slides/'), $fileName);
                    Slide::create(['name' => $fileName, 'user_id' => Auth::id()]);
                }catch(\Exception $e){
                    return $e;
                }
            }else{
                return 'photo extension only: jpg, jpeg, png';
            }
        }
    }
    public function addCategoryTech(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:tech-categories|max:255',
        ]);
        if ($validator->fails()) {
            return response($validator->errors(), 401);
        }
        $category = TechCategories::create(array('name'=>$request->name));
        return $category;
    }

    public function delCategoryTech(Request $request){
        if($request->id){
            TechCategories::where('id', $request->id)->delete();
            Tech::where('category_id', $request->id)->delete();
        }
    }

    public function addTech(Request $request){
        $data['category_id'] = $request->id;
        $data['name'] = $request->name;
        $data['skill'] = $request->skill;
        $tech = Tech::create($data);
        return $tech;
    }

    public function delTech(Request $request){
        if($request->id){
            Tech::where('id', $request->id)->delete();
        }
    }
    public function delWork(Request $request){
        if($request->id){
            Work::where('id', $request->id)->delete();
        }
    }
    public function delSlide(Request $request){
        if($request->id){
            Slide::where('id', $request->id)->delete();
        }
    }

}
