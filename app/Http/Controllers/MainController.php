<?php

namespace App\Http\Controllers;

use App\TechCategories;
use App\Tech;
use Illuminate\Http\Request;
use App\Work;
use App\Slide;

class MainController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        $data['slide'] = Slide::inRandomOrder()->pluck('name')->first();
        $data['works'] = Work::get();
        $data['techs'] = Tech::get();
        return view('main', $data);
    }
}
