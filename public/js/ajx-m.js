/**
 * Created by geferr on 21.02.2017.
 */
$(document).ready(function(){
    $('.work-add').on('click', function () {
        addWork();
        return false;
    });
    $('.addTechcategoryBtn').on('click', function () {
        addTechCategory();
    });
    $('.addTechBtn').on('click', function () {
        console.log($(this));
        addTech($(this).parent().parent());
    });
    $('.techCatRemoveBtn').on('click',function(){
       if(confirm('really will be delete?')){
           deleteTechCat($(this).parent().parent());
       };
    });
    $('.addTechOpen').on('click', function () {
        var id = $(this).parent().parent().find('input[name="_id"]').val();
        $('#addTechForm input[name="_id"]').val(id);
    })
    $('.teachLikeBtn').on('click', function () {
        if(confirm('really will be delete?')){
            deleteTech($(this).attr('tech'), $(this));
        };
    })
    $('.delWork').on('click', function () {
        if(confirm('really will be delete?')){
            deleteWork($(this).parent());
        };
    })
    $('#addSlideBtn').on('change', function () {
        addSlide();
    })
    $('.deleteSlide').on('click', function (e) {
        e.preventDefault();
        if(confirm('really will be delete?')){
            delSlide($(this).attr('slide'));
        };

    })

});

function addWork(){
    var formData = new FormData($('#addWorkF')[0]);

    $.ajax({
        url: '/for-admin-page/addWork/',
        type: 'POST',
        data: formData,
        contentType: false,
        processData: false,
        dataType: 'JSON',
        success: function (data) {
            var work = '<li><div class="user-thumb"> <img width="40" height="40" alt="User" src="img/demo/av1.jpg"> </div>'+
                +'<div class="article-post"> <span class="user-info"> </span>'+
                +'<p><a href="#">'+data.title+'</a></p> </div></li>';
            $('.list-works').prepend(work);
            $.gritter.add({
                title:	'Added new Work',
                text:	data.title+' added!',
                sticky: false
            });
        }
    });
}

function addTechCategory(){
    $.ajax({
        url: '/for-admin-page/addCategoryTech/',
        type: 'POST',
        data: $('#addTechCategoryForm').serialize(),
        dataType: 'JSON',
        success: function (data) {
            console.log(data);
            var categ = '<div class="tech-block"><h5>'+data.name+'</h5><p><a href="#addTech" class="btn btn-mini btn-success" data-toggle="modal">add</a></p><hr></div>';
            $('.teachList').prepend(categ);
            $('#addTechCategoryForm input[name="name"]').val('');
            $.gritter.add({
                title:	'Ajax status',
                text:	'Category for tech added!',
                sticky: false
            });
        }
    });
}

function addTech(el){
    console.log(el);
    var token = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/for-admin-page/addTech/',
        type: 'POST',
        data: {_token: token, name:el.find('input[name="name"]').val(), skill:el.find('input[name="skill"]').val(), id: el.find('input[name="_id"]').val()},
        dataType: 'JSON',
        success: function (data) {
            console.log(data);
            var tech = '<button class="btn btn-mini btn-inverse">'+data.name+'</button>';
            $('.teachList').find('.techs>span').append(tech);
            $('#addTechForm input[name="name"], #addTechForm input[name="skill"]').val('');
            $.gritter.add({
                title:	'Ajax status',
                text:	'Tech added!',
                sticky: false
            });
        }
    });
}

function addSlide(){
    var formData = new FormData($('.add-slide')[0]);
    var token = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/for-admin-page/addSlide/',
        type: 'POST',
        data: formData,
        contentType: false,
        processData: false,
        dataType: 'JSON',
        success: function (data) {

            $.gritter.add({
                title:	'Added new Slide',
                text:	'slide added!',
                sticky: false
            });
        }
    });
}


function deleteTechCat(obj) {
    var token = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/for-admin-page/delCategoryTech/',
        type: 'DELETE',
        data: {_token: token,id:obj.find('input[name="_id"]').val()},
        dataType: 'JSON',
        success: function (data) {
            obj.remove();
            $.gritter.add({
                title:	'Ajax status',
                text:	'Tech cat deleted!',
                sticky: false
            });
        }
    });
}

function deleteTech(id, obj) {
    var token = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/for-admin-page/delTech/',
        type: 'DELETE',
        data: {_token: token,id:id},
        dataType: 'JSON',
        success: function (data) {
            $.gritter.add({
                title:	'Ajax status',
                text:	'Tech deleted!',
                sticky: false
            });
            obj.remove();
        },
        error: function (e) {
            $.gritter.add({
                title:	'Ajax status',
                text:	e,
                sticky: false
            });
        }
    });
}

function deleteWork(obj) {
    var token = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/for-admin-page/delWork/',
        type: 'DELETE',
        data: {_token: token,id:obj.attr('work')},
        dataType: 'JSON',
        success: function (data) {
            $.gritter.add({
                title:	'Ajax status',
                text:	'Work deleted!',
                sticky: false
            });
            obj.remove();
        },
        error: function (e) {
            $.gritter.add({
                title:	'Ajax status',
                text:	e,
                sticky: false
            });
        }
    });
}

function delSlide(id) {
    var token = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: '/for-admin-page/delSlide/',
        type: 'DELETE',
        data: {_token: token,id:id},
        dataType: 'JSON',
        success: function (data) {
            $.gritter.add({
                title:	'Ajax status',
                text:	'Slide deleted!',
                sticky: false
            });
            $('.slide-'+id).remove();
        },
        error: function (e) {
            $.gritter.add({
                title:	'Ajax status',
                text:	e,
                sticky: false
            });
        }
    });
}
