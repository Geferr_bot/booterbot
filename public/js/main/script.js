$(document).ready(function(){

$('li.picnav').hover(
	function(){
		var txt = $(this).find('span').text();
		$('.textinf span').text(txt);
		$('.textinf').stop(true, true).animate({left: 0}, 300);
	},
	function(){
		$('.textinf').stop(true, true).animate({left: '-100%'}, 300);
	});	

$('.donut').peity('donut');
function heig(){
	var hh = $('.head-r').height();
	var th = $('.top-l').height();
	var r = $('.wrap').height();
	var l = $('.wrap').height();

	console.log(r);
	console.log(hh);

	var ch = (r - hh);
	var bh = (l - th);
		console.log(ch);
	$('.content-r').css('height', ch);
	$('.dopinf-l').height(bh);
}
function tabvis(){
	var tab = $('.tabs li.activ').text();
	$('.tab-wind').find('div[wind='+tab+']').fadeIn();
}
tabvis();

$('.tabs li').click(function(){
	$('.tabs li').removeClass('activ');
	$(this).addClass('activ');
	var tab = $(this).text();
	$('.t-wind').fadeOut(0);
	$('.tab-wind').find('div[wind='+tab+']').fadeIn();
})
$('.nav-tl ul li').eq(0).click(function(e){
	e.preventDefault();
	$('#sectWorks .title-sect').animate({opacity: 1}, 1000);
	$('#sectWorks .body-sect').slideDown(500);
})
$('.nav-tl ul li').eq(1).click(function(e){
	e.preventDefault();
	$('#sectBlog .title-sect').animate({opacity: 1}, 1000);
	$('#sectBlog .body-sect').slideDown(500);
	$('#sectWorks .title-sect').animate({opacity: 0.4}, 1000);
	$('#sectAbout .title-sect').animate({opacity: 0.4}, 1000);
	$('#sectWorks .body-sect').slideUp(500);
	$('#sectAbout .body-sect').slideUp(500);
})
$('.nav-tl ul li').eq(3).click(function(e){
	e.preventDefault();
	$('#sectAbout .title-sect').animate({opacity: 1}, 1000);
	$('#sectAbout .body-sect').slideDown(500);
	$('#sectWorks .title-sect').animate({opacity: 0.4}, 1000);
	$('#sectBlog .title-sect').animate({opacity: 0.4}, 1000);
	$('#sectWorks .body-sect').slideUp(500);
	$('#sectBlog .body-sect').slideUp(500);
})
$(window).load(function(){

	heig();
})
});