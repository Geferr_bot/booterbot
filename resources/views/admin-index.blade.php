@extends('layouts.admin')

@section('content')
    <div id="content-header">
        <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
    </div>
    <!--Action boxes-->
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title bg_ly" data-toggle="collapse" href="#collapseG2"><span class="icon"><i class="icon-chevron-down"></i></span>
                        <h5>Latest Posts in blog</h5>
                    </div>
                    <div class="widget-content nopadding collapse in" id="collapseG2">
                        <ul class="recent-posts">
                            <li>
                                <div class="user-thumb"> <img width="40" height="40" alt="User" src="img/demo/av1.jpg"> </div>
                                <div class="article-post"> <span class="user-info"> By: john Deo / Date: 2 Aug 2012 / Time:09:27 AM </span>
                                    <p><a href="#">This is a much longer one that will go on for a few lines.It has multiple paragraphs and is full of waffle to pad out the comment.</a> </p>
                                </div>
                            </li>
                            <li>
                                <button class="btn btn-warning btn-mini">View All</button>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="widget-box">
                    <div class="widget-title bg_ly" data-toggle="collapse" href="#collapseG2"><span class="icon"><i class="icon-chevron-down"></i></span>
                        <h5>Latest works</h5>
                    </div>
                        <ul class="recent-posts list-works">
                            @foreach ($works as $work)
                                <li work="{{$work->id}}">
                                    <button class="btn btn-danger btn-mini delWork fr">delete</button>
                                    @if($work->thumbnail && !empty($work->thumbnail) && $work->thumbnail != Null)
                                        <div class="user-thumb"> <img width="40" height="40" alt="User" src="/img/content/{{$work->thumbnail}}"> </div>
                                    @else
                                        <div class="user-thumb"> <img width="40" height="40" alt="User" src="/img/demo/av1.jpg"> </div>
                                    @endif
                                    <div class="article-post"> <span class="user-info"> {{$work->updated_at}} </span>
                                        <p><a href="#"><h5>{{ $work->title }}</h5></a> </p>
                                    </div>
                                </li>
                            @endforeach

                        </ul>
                        <ul class="recent-posts">
                            <li>
                                <button class="btn btn-warning btn-mini work-view">View All</button>
                                <button class="btn btn-success btn-mini work-add-toggle">Add new Work</button>

                            </li>
                            <li class="addWorkForm" style="display: none;">
                                <form id="addWorkF">
                                    <div class="control-group">
                                        <div class="controls">
                                            <input type="text" class="span12" name="title" placeholder="Title">
                                            <input type="text" class="span12" name="slug" placeholder="Slug">
                                            <input type="text" class="span12" name="excerpt" placeholder="Excerpt">
                                            <input type="text" class="span12" name="link" placeholder="Work Link">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <select multiple style="width: 99%;margin-bottom: 10px;" name="techs">
                                                @foreach ($techs as $t)
                                                    <option value="{{$t['name']}}">{{$t['name']}}</option>
                                                @endforeach
                                            </select>
                                            <div class="uploader" id="uniform-undefined"><input type="file" size="19" name="thumbnail" style="opacity: 0;"><span class="filename">No file selected</span><span class="action">Choose File</span></div>
                                            <textarea class="textarea_editor span12" rows="6" name="content" placeholder="Enter text ..."></textarea>
                                        </div>
                                    </div>
                                    <button class="btn btn-success btn-mini work-add">Save</button>
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="span6" style="margin-left: 0;">
                    <div class="widget-box">
                        <div class="widget-title bg_ly" data-toggle="collapse" href="#collapseG2"><span class="icon"><i class="icon-chevron-down"></i></span>
                            <h5>Tech list</h5>
                        </div>
                        <div class="widget-content collapse in teachList" id="collapseG2">
                            @foreach ($techCat as $tc_val)
                                <div class="tech-block">
                                    <input type="hidden" name="_id" value="{{$tc_val->id}}">
                                    <h5>{{$tc_val->name}}<button class="btn btn-mini fr btn-danger techCatRemoveBtn">remove</button><button class="btn btn-mini btn-warning fr">edit</button></h5>
                                    <p class="techs">
                                <span>
                                    @foreach ($techs as $t)
                                        @if($t['category_id'] == $tc_val->id)
                                            <button class="btn btn-mini btn-inverse teachLikeBtn" tech="{{$t['id']}}">{{$t['name']}}</button>
                                        @endif
                                    @endforeach
                                </span>
                                        <a href="#addTech" class="btn btn-mini btn-success addTechOpen" data-toggle="modal">add</a>
                                    </p>
                                    <hr>
                                </div>
                            @endforeach
                            <a href="#addTechCategory" class="btn btn-mini btn-success" data-toggle="modal">add category</a>
                            <div id="addTechCategory" class="modal hide">
                                <div class="modal-header">
                                    <button data-dismiss="modal" class="close" type="button">×</button>
                                    <h3>Add Tech Category</h3>
                                </div>
                                <div class="modal-body">
                                    <form id="addTechCategoryForm" action="#">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="text" class="span12" name="name" placeholder="name">
                                    </form>
                                </div>
                                <div class="modal-footer"> <button class="btn btn-primary addTechcategoryBtn">Save</button> <a data-dismiss="modal" class="btn" href="#">Cancel</a> </div>
                            </div>
                            <div id="addTech" class="modal hide">
                                <div class="modal-header">
                                    <button data-dismiss="modal" class="close" type="button">×</button>
                                    <h3>Add Tech for <span></span></h3>
                                </div>
                                <div class="modal-body">
                                    <form id="addTechForm" action="#">
                                        <input type="hidden" name="_id" value="">
                                        <input type="text" class="span12" name="name" placeholder="name">
                                        <input type="number" class="span12" name="skill" placeholder="skill %">
                                    </form>
                                </div>
                                <div class="modal-footer"> <a class="btn btn-primary addTechBtn" href="#">Save</a> <a data-dismiss="modal" class="btn" href="#">Cancel</a> </div>
                            </div>
                        </div>
                    </div>
                </div>
            <div class="span6">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"><i class="icon-ok"></i></span>
                        <h5>Skills</h5>
                    </div>
                    <div class="widget-content">
                        <ul class="unstyled">
                            @foreach ($techs as $t)
                                @if(!empty($t['skill']))
                                    <li> <span class="icon24 icomoon-icon-arrow-up-2 green"></span>{{$t['name']}} - {{$t['skill']}}%<span class="pull-right strong">567</span>
                                        <div class="progress progress-striped {{$t['skill-color']}}">
                                            <div style="width: {{$t['skill']}}%;" class="bar"></div>
                                        </div>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <div class="span12" style="margin-left: 0;">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"><i class="icon-ok"></i></span>
                        <h5>To Do list</h5>
                    </div>
                    <div class="widget-content">
                        <div class="todo">
                            <ul>
                                <li class="clearfix">
                                    <div class="txt"> Luanch This theme on Themeforest <span class="by label">Alex</span></div>
                                    <div class="pull-right"> <a class="tip" href="#" title="Edit Task"><i class="icon-pencil"></i></a> <a class="tip" href="#" title="Delete"><i class="icon-remove"></i></a> </div>
                                </li>
                                <li class="clearfix">
                                    <div class="txt"> Manage Pending Orders <span class="date badge badge-warning">Today</span> </div>
                                    <div class="pull-right"> <a class="tip" href="#" title="Edit Task"><i class="icon-pencil"></i></a> <a class="tip" href="#" title="Delete"><i class="icon-remove"></i></a> </div>
                                </li>
                                <li class="clearfix">
                                    <div class="txt"> MAke your desk clean <span class="by label">Admin</span></div>
                                    <div class="pull-right"> <a class="tip" href="#" title="Edit Task"><i class="icon-pencil"></i></a> <a class="tip" href="#" title="Delete"><i class="icon-remove"></i></a> </div>
                                </li>
                                <li class="clearfix">
                                    <div class="txt"> Today we celebrate the theme <span class="date badge badge-info">08.03.2013</span> </div>
                                    <div class="pull-right"> <a class="tip" href="#" title="Edit Task"><i class="icon-pencil"></i></a> <a class="tip" href="#" title="Delete"><i class="icon-remove"></i></a> </div>
                                </li>
                                <li class="clearfix">
                                    <div class="txt"> Manage all the orders <span class="date badge badge-important">12.03.2013</span> </div>
                                    <div class="pull-right"> <a class="tip" href="#" title="Edit Task"><i class="icon-pencil"></i></a> <a class="tip" href="#" title="Delete"><i class="icon-remove"></i></a> </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="widget-box">
                    <div class="widget-title">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#tab1">General</a></li>
                            <li><a data-toggle="tab" href="#tab2">Slides</a></li>
                            <li><a data-toggle="tab" href="#tab3">Colors</a></li>
                        </ul>
                    </div>
                    <div class="widget-content tab-content">

                            <div id="tab1" class="tab-pane active">
                                <form action="#" method="get" id="f-sett-gen" class="">
                                    <div class="widget-content nopadding">
                                            <div class="control-group">
                                                <label class="control-label">Title:</label>
                                                <div class="controls">
                                                    <input type="text" class="span11" name="title" />
                                                </div>
                                            </div>
                                    </div>
                                </form>
                            </div>
                            <div id="tab2" class="tab-pane">
                                <ul class="thumbnails thumbn-list">
                                    @foreach ($slides as $s)
                                    <li class="span2 slide-{{$s->id}}"> <a> <img src="/img/slides/{{$s->name}}" alt="" > </a>
                                        <div class="actions">
                                            <a title="" class="" href="#"><i class="icon-pencil"></i></a>
                                            <a class="lightbox_trigger" href="/img/slides/{{$s->name}}"><i class="icon-search"></i></a>
                                            <a title="" class="deleteSlide" slide="{{$s->id}}" href="#"><i class="icon-trash"></i></a>
                                        </div>
                                    </li>
                                    @endforeach
                                </ul>
                                <ul class="thumbnails thumb-add-slides">
                                    <li class="span2" id="add-img"><i class="icon icon-plus"></i>
                                        <form action="#" class="add-slide">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <div class="control-group">
                                                <div class="controls">
                                                    <div class="uploader" id="uniform-undefined">
                                                        <input type="file" size="19" name="slide" id="addSlideBtn" style="opacity: 0;">
                                                        <span class="filename"></span>
                                                        <span class="action"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </li>
                                </ul>
                                <button class="btn btn-success btn-mini slide-add">Save</button>
                            </div>
                            <div id="tab3" class="tab-pane">
                                <form class="">
                                    <div class="control-group">
                                        <label class="control-label">Left Sidebar</label>
                                        <div class="controls">
                                            <div data-color-format="hex" data-color="#000000"  class="input-append color colorpicker">
                                                <input type="text" value="#000000" class="span11">
                                                <span class="add-on"><i style="background-color: #000000"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
