@extends('layouts.admin')

@section('content')
    <div id="content-header">
        <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
    </div>
    <!--Action boxes-->
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title bg_ly" data-toggle="collapse" href="#collapseG2"><span class="icon"><i class="icon-chevron-down"></i></span>
                        <h5>Latest Posts in blog</h5>
                    </div>
                    <div class="widget-content nopadding collapse in" id="collapseG2">
                        <ul class="recent-posts">
                            <li>
                                <div class="user-thumb"> <img width="40" height="40" alt="User" src="img/demo/av1.jpg"> </div>
                                <div class="article-post"> <span class="user-info"> By: john Deo / Date: 2 Aug 2012 / Time:09:27 AM </span>
                                    <p><a href="#">This is a much longer one that will go on for a few lines.It has multiple paragraphs and is full of waffle to pad out the comment.</a> </p>
                                </div>
                            </li>
                            <li>
                                <button class="btn btn-warning btn-mini">View All</button>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="widget-box">
                    <div class="widget-title bg_ly" data-toggle="collapse" href="#collapseG2"><span class="icon"><i class="icon-chevron-down"></i></span>
                        <h5>Latest works</h5>
                    </div>
                    <div class="widget-content nopadding collapse in" id="collapseG2">
                        <ul class="recent-posts">
                            <li>
                                <div class="user-thumb"> <img width="40" height="40" alt="User" src="img/demo/av1.jpg"> </div>
                                <div class="article-post"> <span class="user-info"> By: john Deo / Date: 2 Aug 2012 / Time:09:27 AM </span>
                                    <p><a href="#">This is a much longer one that will go on for a few lines.It has multiple paragraphs and is full of waffle to pad out the comment.</a> </p>
                                </div>
                            </li>
                            <li>
                                <button class="btn btn-warning btn-mini work-view">View All</button>
                                <a href="#addWork" class="btn btn-success btn-mini work-add" data-toggle="modal">Add new Work</a>
                                <div id="addWork" class="modal hide" aria-hidden="true" style="display: none;">
                                    <div class="modal-header">
                                        <button data-dismiss="modal" class="close" type="button">×</button>
                                        <h3>Add new Work</h3>
                                    </div>
                                    <div class="modal-body">
                                        <div class="widget-content nopadding">
                                            <form action="#" method="get" class="">
                                                <div class="control-group">
                                                    <div class="controls">
                                                        <input type="text" class="span11" placeholder="Title">
                                                        <input type="text" class="span11" placeholder="Slug">
                                                        <input type="text" class="span11" placeholder="Excerpt">
                                                        <select multiple >
                                                            <option>First option</option>
                                                            <option selected>Second option</option>
                                                            <option>Third option</option>
                                                            <option>Fourth option</option>
                                                            <option>Fifth option</option>
                                                            <option>Sixth option</option>
                                                            <option>Seventh option</option>
                                                            <option>Eighth option</option>
                                                        </select>
                                                        <div class="uploader" id="uniform-undefined"><input type="file" size="19" style="opacity: 0;"><span class="filename">No file selected</span><span class="action">Choose File</span></div>
                                                        <textarea class="textarea_editor span12" rows="6" placeholder="Enter text ..."></textarea>
                                                    </div>
                                                </div>


                                            </form>
                                        </div>
                                    </div>
                                    <div class="modal-footer"> <a data-dismiss="modal" class="btn btn-primary" href="#">Save</a> <a data-dismiss="modal" class="btn" href="#">Cancel</a> </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="widget-box">
                    <div class="widget-title bg_ly" data-toggle="collapse" href="#collapseG2"><span class="icon"><i class="icon-chevron-down"></i></span>
                        <h5>Tech list</h5>
                    </div>
                    <div class="widget-content collapse in" id="collapseG2">
                        <div class="tech-block">
                            <h5>Backend(Web)</h5>
                            <p>
                                <button class="btn btn-mini btn-inverse">php</button>
                                <button class="btn btn-mini btn-inverse">laravel</button>
                                <button class="btn btn-mini btn-success">add</button>
                            </p>
                            <hr>
                        </div>
                        <button class="btn btn-success">add category</button>
                    </div>
                </div>
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"><i class="icon-ok"></i></span>
                        <h5>To Do list</h5>
                    </div>
                    <div class="widget-content">
                        <div class="todo">
                            <ul>
                                <li class="clearfix">
                                    <div class="txt"> Luanch This theme on Themeforest <span class="by label">Alex</span></div>
                                    <div class="pull-right"> <a class="tip" href="#" title="Edit Task"><i class="icon-pencil"></i></a> <a class="tip" href="#" title="Delete"><i class="icon-remove"></i></a> </div>
                                </li>
                                <li class="clearfix">
                                    <div class="txt"> Manage Pending Orders <span class="date badge badge-warning">Today</span> </div>
                                    <div class="pull-right"> <a class="tip" href="#" title="Edit Task"><i class="icon-pencil"></i></a> <a class="tip" href="#" title="Delete"><i class="icon-remove"></i></a> </div>
                                </li>
                                <li class="clearfix">
                                    <div class="txt"> MAke your desk clean <span class="by label">Admin</span></div>
                                    <div class="pull-right"> <a class="tip" href="#" title="Edit Task"><i class="icon-pencil"></i></a> <a class="tip" href="#" title="Delete"><i class="icon-remove"></i></a> </div>
                                </li>
                                <li class="clearfix">
                                    <div class="txt"> Today we celebrate the theme <span class="date badge badge-info">08.03.2013</span> </div>
                                    <div class="pull-right"> <a class="tip" href="#" title="Edit Task"><i class="icon-pencil"></i></a> <a class="tip" href="#" title="Delete"><i class="icon-remove"></i></a> </div>
                                </li>
                                <li class="clearfix">
                                    <div class="txt"> Manage all the orders <span class="date badge badge-important">12.03.2013</span> </div>
                                    <div class="pull-right"> <a class="tip" href="#" title="Edit Task"><i class="icon-pencil"></i></a> <a class="tip" href="#" title="Delete"><i class="icon-remove"></i></a> </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"><i class="icon-ok"></i></span>
                        <h5>Skills</h5>
                    </div>
                    <div class="widget-content">
                        <ul class="unstyled">
                            <li> <span class="icon24 icomoon-icon-arrow-up-2 green"></span> 81% Clicks <span class="pull-right strong">567</span>
                                <div class="progress progress-striped ">
                                    <div style="width: 81%;" class="bar"></div>
                                </div>
                            </li>
                            <li> <span class="icon24 icomoon-icon-arrow-up-2 green"></span> 72% Uniquie Clicks <span class="pull-right strong">507</span>
                                <div class="progress progress-success progress-striped ">
                                    <div style="width: 72%;" class="bar"></div>
                                </div>
                            </li>
                            <li> <span class="icon24 icomoon-icon-arrow-down-2 red"></span> 53% Impressions <span class="pull-right strong">457</span>
                                <div class="progress progress-warning progress-striped ">
                                    <div style="width: 53%;" class="bar"></div>
                                </div>
                            </li>
                            <li> <span class="icon24 icomoon-icon-arrow-up-2 green"></span> 3% Online Users <span class="pull-right strong">8</span>
                                <div class="progress progress-danger progress-striped ">
                                    <div style="width: 3%;" class="bar"></div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="widget-box">
                    <div class="widget-title">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#tab1">General</a></li>
                            <li><a data-toggle="tab" href="#tab2">Slides</a></li>
                            <li><a data-toggle="tab" href="#tab3">Colors</a></li>
                        </ul>
                    </div>
                    <div class="widget-content tab-content">

                            <div id="tab1" class="tab-pane active">
                                <form action="#" method="get" id="f-sett-gen" class="">
                                    <div class="widget-content nopadding">
                                            <div class="control-group">
                                                <label class="control-label">Title:</label>
                                                <div class="controls">
                                                    <input type="text" class="span11" name="title" />
                                                </div>
                                            </div>
                                    </div>
                                </form>
                            </div>
                            <div id="tab2" class="tab-pane">
                                <ul class="thumbnails">
                                    <li class="span2"> <a> <img src="img/gallery/imgbox3.jpg" alt="" > </a>
                                        <div class="actions">
                                            <a title="" class="" href="#"><i class="icon-pencil"></i></a>
                                            <a class="lightbox_trigger" href="img/gallery/imgbox3.jpg"><i class="icon-search"></i></a>
                                            <a title="" class="" href="#"><i class="icon-trash"></i></a>
                                        </div>
                                    </li>


                                    <li class="span2" id="add-img"><i class="icon icon-plus"></i>
                                        <form action="#" method="get" class="">
                                        <div class="control-group">
                                            <div class="controls">
                                                <div class="uploader" id="uniform-undefined">
                                                    <input type="file" size="19" style="opacity: 0;">
                                                    <span class="filename"></span>
                                                    <span class="action"></span>
                                                </div>
                                            </div>
                                        </div>
                                        </form>
                                    </li>
                                </ul>
                            </div>
                            <div id="tab3" class="tab-pane">
                                <form class="">
                                    <div class="control-group">
                                        <label class="control-label">Left Sidebar</label>
                                        <div class="controls">
                                            <input type="text" data-color="#ffffff" value="#ffffff" class="colorpicker input-big span11">
                                            <span class="help-block">Color picker for left sidebar</span> </div>
                                    </div>
                                </form>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
