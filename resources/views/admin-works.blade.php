@extends('layouts.admin')

@section('content')
    <div id="content-header">
        <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
    </div>
    <!--Action boxes-->
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title bg_ly" data-toggle="collapse" href="#collapseG2"><span class="icon"><i class="icon-chevron-down"></i></span>
                        <h5>Works</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <ul class="recent-posts">
                            @foreach ($works as $work)
                            <li>
                                @if($work->thumbnail && !empty($work->thumbnail) && $work->thumbnail != Null)
                                <div class="user-thumb"> <img width="40" height="40" alt="User" src="/img/content/{{$work->thumbnail}}"> </div>
                                @else
                                <div class="user-thumb"> <img width="40" height="40" alt="User" src="/img/demo/av1.jpg"> </div>
                                @endif
                                <div class="article-post">
                                    <div class="fr"><a href="#" class="btn btn-primary btn-mini">Edit</a> <a href="#" class="btn btn-success btn-mini">Publish</a> <a href="#" class="btn btn-danger btn-mini">Delete</a></div>
                                    <span class="user-info">{{$work->updated_at}}</span>
                                    <p><a href="#"><h5>{{$work->title}}</h5></a> </p>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                        <button class="btn btn-warning btn-mini">View More</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
