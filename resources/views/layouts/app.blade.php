<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link rel="shortcut icon" href="{{ asset('/favicon.ico') }}" type="image/png">
    <link rel="stylesheet" href="{{ asset('/css/main/style.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/main/general.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/main/jquery.mCustomScrollbar.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/main/chartist.min.css') }}">
{{--<link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}

<!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
@if(isset($slide))
    <img src="/img/slides/{{$slide}}" alt="">
@endif
<div class="wrap">
    <div class="cont-wrap">
        <div class="left tc">
            <div class="top-l">
                <div class="tl-upp">
                    <div class="forlog">
                        <div class="logo">
                            <img src="{{ asset('/img/main/logo.png')}}" alt="booterbot.com">
                        </div>
                        <div class="nav-tl">
                            <ul>
                                <li class="picnav flaticon-home166 activ"><a href="#"></a><span>Домой</span></li>
                                <li class="picnav flaticon-pencil124"><a href="#"></a><span>Блог</span></li>
                                <li class="picnav flaticon-book244"><a href="#"></a><span>Резюме</span></li>
                                <li class="picnav flaticon-profile29"><a href="#"></a><span>Кто я</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="int-l">
                        <div class="titl-l">Деятельность</div>
                        <ul>
                            <li class="int">{{count($works)}}<span>Работ</span></li>
                            <li class="int">{{count($works)}}<span>Клиентов</span></li>
                            <li class="int">{{count($techs)}}<span>Технологии</span></li>
                        </ul>
                    </div>
                    <div class="chart-l">
                        <div class="titl-l">Скилы</div>
                        <div class="char-skill">
                            <div class="left-cs">php</div>
                            <div class="right-cs">
                                <div class="chrt">
                                    <span class="donut" data-peity='{ "fill": ["green", "#eeeeee"],  "innerRadius": 18, "radius": 24 }'>25/100</span>
                                    <p>test</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="dopinf-l">
                <div class="sect-dl">
                </div>
                <div class="soc">
                    <div class="soc-b">
                        <div class="socim">
                            <img src="{{ asset('/img/main/skype.png')}}" alt="">
                        </div>
                        <span>geferrr</span>
                    </div>
                    <div class="soc-b">
                        <div class="socim">
                            <img src="{{ asset('/img/main/mail.png')}}" alt="">
                        </div>
                        <span>androstand@gmail.com</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="right tc">
            <div class="infobtn">
                <div class="textinf"><span>Домой</span></div>
            </div>
            <div class="head-r">
                <div class="name-auth">
                    <h1>Sergey Kovalchuk</h1>
                    <h2>Web Developer</h2>
                </div>
                <div class="langs">
                    <a class="flaticon-pin60" href="#"></a>
                    <a class="activ"href="#">Ru</a>
                    <a href="#">Eng</a>
                </div>
            </div>
            <div class="content-r mCustomScrollbar" data-mcs-theme="dark">
                <div class="body">
                    {{--<section id="currentWork">--}}
                        {{--<div class="title-sect">Над чем сейчас работаю</div>--}}
                        {{--<div class="body-sect">--}}
                            {{--@yield('content')--}}
                            {{--<a href="http://geferr.byethost7.com/"><img src="/img/Screenshot_2.png" alt=""></a>--}}
                            {{--<div class="info">Работаю на <a href="http://geferr.byethost7.com/">магазином</a> на cms Opencart.</div>--}}
                        {{--</div>--}}
                    {{--</section>--}}
                    <section id="sectWorks">
                        <div class="title-sect">Мои работы</div>
                        <div class="body-sect">
                            <div class="tabs">
                                <ul>
                                    <li class="activ">Все</li>

                                </ul>
                            </div>
                            <div class="tab-wind">
                                <div wind="Все" class="t-wind" style="display:none;">
                                @foreach($works as $w)
                                    <div class="work-c">
                                        <img src="/img/content/{{$w->thumbnail}}" alt="">
                                        <div class="datainf" style="display:none;">
                                            <div class="name-wc">{{$w->title}}</div>
                                            <a href="http://<?php //echo get_post_meta($post->ID, 'link_p', true);?>"  target="_blank"><?php //echo get_post_meta($post->ID, 'link_p', true);?></a>
                                            <div class="icons-wc">
                                                <div class="icon-wc "></div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                </div>
                                <div wind="3" class="t-wind" style="display:none;">
                                    <div class="work-c">
                                        <?php //echo get_the_post_thumbnail( $pagex->ID, 'medium' ); ?>
                                        <div class="datainf" style="display:none;">
                                            <div class="name-wc"><?php //echo $pagex->post_title; ?></div>
                                            <a href="http://<?php //echo get_post_meta($pagex->ID, 'link_p', true);?>"  target="_blank"><?php //echo get_post_meta($pagex->ID, 'link_p', true);?></a>
                                            <div class="icons-wc">
                                                <div class="icon-wc "></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section id="sectBlog">
                        <div class="title-sect">Записи блога</div>
                        <div class="body-sect">
                            <div class="postbl">
                                <div class="left-pb">
                                    <?php //echo get_the_post_thumbnail( $post->ID, 'medium' ); ?>
                                </div>
                                <div class="right-pb">
                                    <div class="author-time">
                                        <div class="author-pb">author</div>
                                        <div class="time-pb"><?php //the_time('j F Y');?></div>
                                    </div>
                                    <div class="titl-pb">title</div>
                                    <div class="desc-pb">content</div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section id="sectAbout">
                        <div class="title-sect">Обо мне</div>
                        <div class="body-sect">
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<!-- Scripts -->
<script src="{{ asset('js/main/jquery.js') }}"></script>
<script src="{{ asset('js/main/jquery.mCustomScrollbar.js') }}"></script>
<script src="{{ asset('js/main/jquery.peity.js') }}"></script>
<script src="{{ asset('js/main/script.js') }}"></script>
</body>
</html>
