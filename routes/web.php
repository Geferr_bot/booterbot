<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'MainController@index');


Auth::routes();

Route::get('/for-admin-page', 'AdminController@index');
Route::get('/for-admin-page/pages', 'AdminController@ListPages');
Route::get('/for-admin-page/blog', 'AdminController@ListPosts');
Route::get('/for-admin-page/works', 'AdminController@ListWorks');
Route::get('/for-admin-page/skills', 'AdminController@Skills');
Route::get('/for-admin-page/tasks', 'AdminController@Tasks');
Route::get('/for-admin-page/settings', 'AdminController@Settings');


//Ajax
Route::post('/for-admin-page/addWork', 'AjaxController@addWork');
Route::post('/for-admin-page/addCategoryTech', 'AjaxController@addCategoryTech');
Route::post('/for-admin-page/addTech', 'AjaxController@addTech');
Route::post('/for-admin-page/addSlide', 'AjaxController@addSlide');
Route::delete('/for-admin-page/delCategoryTech', 'AjaxController@delCategoryTech');
Route::delete('/for-admin-page/delTech', 'AjaxController@delTech');
Route::delete('/for-admin-page/delWork', 'AjaxController@delWork');
Route::delete('/for-admin-page/delSlide', 'AjaxController@delSlide');